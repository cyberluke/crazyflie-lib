/* 
 *  Copyright (C) 2014 Andreas Huber
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
package se.bitcraze.crazyflie.crtp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import se.bitcraze.crazyflie.crtp.Crtp.CommandPacket;

/**
 * @author Andreas Huber
 * 
 */
public class DebugDriver extends AbstractDriver {

	public static final String URI = "debug://0/0";
	private static final Logger log = LoggerFactory
			.getLogger(DebugDriver.class);
	private boolean connected;

	/**
	 * 
	 */
	public DebugDriver() {
	}

	public static final String[] scanInterfaces() {
		return new String[] { URI };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see se.bitcraze.crazyflie.Link#connect()
	 */
	@Override
	public void connect() throws Exception {
		connected = true;
		log.info(URI + " connected");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see se.bitcraze.crazyflie.Link#disconnect()
	 */
	@Override
	public void disconnect() throws Exception {
		connected = false;
		log.info(URI + " disconnected");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see se.bitcraze.crazyflie.Link#isConnected()
	 */
	@Override
	public boolean isConnected() {
		return connected;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * se.bitcraze.crazyflie.Link#send(se.bitcraze.crazyflie.crtp.CrtpPacket)
	 */
	@Override
	public void sendAsync(Crtp.Request p) {
		if (p instanceof Crtp.CommandPacket)
			debugCommander((CommandPacket) p);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * se.bitcraze.crazyflie.crtp.CrtpDriver#send(se.bitcraze.crazyflie.crtp
	 * .Crtp.Request)
	 */
	@Override
	public <T extends Crtp.Response> T send(Crtp.Request request) {
		// TODO Auto-generated method stub
		return null;
	}

	private void debugCommander(Crtp.CommandPacket packet) {
		log.info("{} roll: {}, pitch: {}, yaw: {}, thrust: {}", URI,
				packet.getRoll(), packet.getPitch(), packet.getYaw(),
				(int) packet.getThrust());
	}

}
