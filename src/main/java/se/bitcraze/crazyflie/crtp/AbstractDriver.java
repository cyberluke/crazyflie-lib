/* 
 *  Copyright (C) 2014 Andreas Huber
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package se.bitcraze.crazyflie.crtp;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import se.bitcraze.crazyflie.ConnectionListener;
import se.bitcraze.crazyflie.crtp.Crtp.Response;

/**
 * This class provides a skeletal implementation of the {@link CrtpDriver}
 * interface to minimize the effort required to implement the interface.
 */
public abstract class AbstractDriver implements CrtpDriver {

	private List<ConnectionListener> connectionListeners;
	private List<DataListener> dataListeners;

	/**
	 * Create a new abstract link.
	 */
	public AbstractDriver() {
		this.connectionListeners = new CopyOnWriteArrayList<ConnectionListener>();
		this.dataListeners = new CopyOnWriteArrayList<DataListener>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * se.bitcraze.crazyflielib.Link#addConnectionListener(se.bitcraze.crazyflielib
	 * .ConnectionListener)
	 */
	@Override
	public void addListener(ConnectionListener l) {
		this.connectionListeners.add(l);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see se.bitcraze.crazyflielib.Link#removeConnectionListener(se.bitcraze.
	 * crazyflielib.ConnectionListener)
	 */
	@Override
	public void removeListener(ConnectionListener l) {
		this.connectionListeners.remove(l);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * se.bitcraze.crazyflielib.Link#addDataListener(se.bitcraze.crazyflielib
	 * .DataListener)
	 */
	@Override
	public void addListener(DataListener l) {
		this.dataListeners.add(l);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * se.bitcraze.crazyflielib.Link#removeDataListener(se.bitcraze.crazyflielib
	 * .DataListener)
	 */
	@Override
	public void removeListener(DataListener l) {
		this.dataListeners.remove(l);
	}

	/**
	 * Handle the response from the Crazyflie. Parses the CRPT packet and inform
	 * registered listeners.
	 * 
	 * @param data
	 *            the data received from the Crazyflie. Must not include any
	 *            headers or other attachments added by the link.
	 */
	protected Crtp.Response handleResponse(byte[] data) {
		if (data.length >= 1) {
			Response response = Crtp.parseResponse(data);
			if (response != null && response.isNotification()) {
				notifyDataReceived(response);
				return null;
			}
			return response;
		}
		return null;
	}

	/**
	 * Notify all registered listeners about an initiated connection.
	 */
	protected void notifyConnectionInitiated() {
		for (ConnectionListener cl : this.connectionListeners) {
			cl.connectionInitiated(this);
		}
	}

	/**
	 * Notify all registered listeners about a setup connection.
	 */
	protected void notifyConnectionSetupFinished() {
		for (ConnectionListener cl : this.connectionListeners) {
			cl.connectionSetupFinished(this);
		}
	}

	/**
	 * Notify all registered listeners about a disconnect.
	 */
	protected void notifyDisconnected() {
		for (ConnectionListener cl : this.connectionListeners) {
			cl.disconnected(this);
		}
	}

	/**
	 * Notify all registered listeners about a lost connection.
	 */
	protected void notifyConnectionLost() {
		for (ConnectionListener cl : this.connectionListeners) {
			cl.connectionLost(this);
		}
	}

	/**
	 * Notify all registered listeners about a failed connection attempt.
	 */
	protected void notifyConnectionFailed() {
		for (ConnectionListener cl : this.connectionListeners) {
			cl.connectionFailed(this);
		}
	}

	/**
	 * Notify all registered listeners about the link status.
	 * 
	 * @param quality
	 *            quality of the link
	 * @see ConnectionListener#linkQualityUpdate(CrtpDriver, int)
	 */
	protected void notifyLinkQuality(int quality) {
		for (ConnectionListener cl : this.connectionListeners) {
			cl.linkQualityUpdate(this, quality);
		}
	}

	protected void notifyDataReceived(Crtp.Response response) {
		synchronized (this.dataListeners) {
			for (DataListener cl : this.dataListeners) {
				cl.dataReceived(this, response);
			}
		}
	}
}
