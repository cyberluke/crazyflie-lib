/* 
 *  Copyright (C) 2014 Andreas Huber
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package se.bitcraze.crazyflie;

import se.bitcraze.crazyflie.crtp.CrtpDriver;

/**
 * An abstract adapter class for receiving connection events. The methods in
 * this class are empty. This class exists as convenience for creating listener
 * objects.
 */
public abstract class ConnectionAdapter implements ConnectionListener {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * se.bitcraze.crazyflielib.ConnectionListener#connectionInitiated(se.bitcraze
	 * .crazyflielib.Link)
	 */
	@Override
	public void connectionInitiated(CrtpDriver driver) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * se.bitcraze.crazyflielib.ConnectionListener#connectionSetupFinished(se
	 * .bitcraze.crazyflielib.Link)
	 */
	@Override
	public void connectionSetupFinished(CrtpDriver driver) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * se.bitcraze.crazyflielib.ConnectionListener#disconnected(se.bitcraze.
	 * crazyflielib.Link)
	 */
	@Override
	public void disconnected(CrtpDriver driver) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * se.bitcraze.crazyflielib.ConnectionListener#connectionLost(se.bitcraze
	 * .crazyflielib.Link)
	 */
	@Override
	public void connectionLost(CrtpDriver driver) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * se.bitcraze.crazyflielib.ConnectionListener#connectionFailed(se.bitcraze
	 * .crazyflielib.Link)
	 */
	@Override
	public void connectionFailed(CrtpDriver driver) {
	}

	@Override
	public void linkQualityUpdate(CrtpDriver driver, int quality) {
	}

}
