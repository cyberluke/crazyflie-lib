/* 
 *  Copyright (C) 2014 Andreas Huber
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package se.bitcraze.crazyflie;

import java.io.Serializable;
import java.util.Arrays;

import org.apache.commons.lang3.ArrayUtils;

/**
 * @author Andreas Huber
 * 
 */
public class LogConfig implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8331011835591306449L;
	private String name;

	private final int groupId;
	private final int interval;
	private volatile String[] variableNames;
	private final Object sync = new Object();
	private volatile boolean active;
	private volatile LogVariable[] variables;

	/**
	 * 
	 */
	LogConfig(String name, int interval, String... variableNames) {
		this.name = name;
		this.groupId = Logging.getBlockId();
		this.interval = interval;
		this.variableNames = variableNames;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the groupId
	 */
	public int getGroupId() {
		return groupId;
	}

	/**
	 * @return the interval
	 */
	public int getInterval() {
		return interval;
	}

	public String[] getVariableNames() {
		return variableNames;
	}

	void addVariable(String variableName) {
		synchronized (sync) {
			if (!ArrayUtils.contains(variableNames, variableName))
				variableNames = ArrayUtils.add(variableNames, variableName);
		}
	}

	public LogVariable[] getVariables() {
		return variables;
	}

	public void setVariables(LogVariable[] variables) {
		this.variables = variables;
	}

	public int getCount() {
		return ArrayUtils.getLength(variableNames);
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean started) {
		this.active = started;
	}

	@Override
	public String toString() {
		return "LogConfig [name=" + name + ", groupId=" + groupId
				+ ", interval=" + interval + ", variables="
				+ Arrays.toString(variableNames) + ", started=" + active + "]";
	}

}
