/* 
 *  Copyright (C) 2014 Andreas Huber
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package se.bitcraze.crazyflie;

import se.bitcraze.crazyflie.crtp.CrtpDriver;

/**
 * Interface for receiving notifications about the connection status of a
 * {@link CrtpDriver}.
 */
public interface ConnectionListener {

	/**
	 * Called when a request has been made to the library to establish a
	 * connection.
	 * 
	 * @param driver
	 *            the link where the request has been made
	 */
	public void connectionInitiated(CrtpDriver driver);

	/**
	 * Called when the connection has been established and the log/param TOC has
	 * been downloaded.
	 * 
	 * @param driver
	 *            the link where the connection has been established
	 */
	public void connectionSetupFinished(CrtpDriver driver);

	/**
	 * Called when the connection has been closed (both when requested and not
	 * requested to close).
	 * 
	 * @param driver
	 *            the link which was disconnected
	 */
	public void disconnected(CrtpDriver driver);

	/**
	 * Called when the connection has been closed (without being requested to be
	 * closed).
	 * 
	 * @param driver
	 *            the link where the connection has been closed
	 */
	public void connectionLost(CrtpDriver driver);

	/**
	 * Called if the connection fails when it is being established (between the
	 * request and connection setup finished).
	 * 
	 * @param driver
	 *            the link where the connection has failed
	 */
	public void connectionFailed(CrtpDriver driver);

	/**
	 * Called periodically to report link status. The quantity of updates is at
	 * the discretion of the link implementation.
	 * 
	 * @param driver
	 *            the link which reports the link status.
	 * @param quality
	 *            the quality in range from 0-100. 0 means bad quality, 100 is
	 *            best quality.
	 */
	public void linkQualityUpdate(CrtpDriver driver, int quality);
}
