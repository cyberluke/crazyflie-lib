package se.bitcraze.crazyflie;

import se.bitcraze.crazyflie.crtp.CrtpDriver;

public interface ConsoleListener {

	public void messageReceived(CrtpDriver driver, String message);
}
