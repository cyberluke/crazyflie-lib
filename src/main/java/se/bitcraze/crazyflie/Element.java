/* 
 *  Copyright (C) 2014 Andreas Huber
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
package se.bitcraze.crazyflie;

import java.io.Serializable;

/**
 * @author Andreas Huber
 * 
 */
public abstract class Element implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7648641382465544105L;
	private final int id;
	private final int type;
	private final String group;
	private final String variable;
	private Object value;

	/**
	 * 
	 */
	public Element(int id, int type, String group, String variable) {
		this.id = id;
		this.type = type;
		this.group = group;
		this.variable = variable;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}

	/**
	 * @return the group
	 */
	public String getGroup() {
		return group;
	}

	/**
	 * @return the name
	 */
	public String getVariable() {
		return variable;
	}

	public abstract int getLength();

	/**
	 * @return the value
	 */
	public Object getValue() {
		return value;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	void setValue(Object value) {
		this.value = value;
	}

	public String getName() {
		return this.group + "." + this.variable;
	}
}
